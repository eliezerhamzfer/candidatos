#encoding:utf-8
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'Candidatos.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    #url(r'^inicio/', views.index, name='index'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^www/', include('www.urls')),
)

# Condición de poder ver los archivos media en desarrollo
if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': '/'.join([settings.BASE_DIR, 'media'])}),
    )