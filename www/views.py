# coding:utf-8
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from www.models import *
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
def index(request):
 	ciudades = Ciudad.objects.all()
	template='main.html'
	scope = {
		'ciudades':ciudades
	}

	return render(request, template, scope)

def ciudad(request, ciudad_id):
 	ciudad = get_object_or_404(Ciudad, pk=ciudad_id)
	template='ciudad.html'
	scope = {
		'ciudad': ciudad
	}
	
	return render(request, template, scope)

def candidato(request, ciudad_id):
	candidatos = get_object_or_404(Candidato, pk = ciudad_id)
	template='candidatos.html'
	scope = {
		'candidatos': candidatos
	}
	
	return render(request, template, scope)