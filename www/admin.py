#encoding:utf-8
from django.contrib import admin
from www.models import *
# Register your models here.
admin.site.register(RedSocial)
admin.site.register(Pais)
admin.site.register(Provincia)
admin.site.register(Ciudad)
admin.site.register(Partido)
admin.site.register(Candidato)