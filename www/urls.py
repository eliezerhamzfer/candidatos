# coding:utf8
from django.conf.urls import patterns, include, url
from django.contrib import admin
from www import views

urlpatterns = patterns('',
	url(r'^$', views.index, name='index'), 
	url(r'^(?P<ciudad_id>\d+)/$', views.ciudad, name='ciudad'),
	url(r'^(?P<ciudad_id>\d+)/candidatos/$', views.candidato, name='candidato'),
)