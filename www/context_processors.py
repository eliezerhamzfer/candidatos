# coding:utf8
from django.conf import settings
from www.models import *

def LastActivity(request):
	#un objeto vacio ya que algunas veces lo devolveremos con datos y otras vacio
	result = {}
	#
	if 'ciudad' in request.session:
		#obtenemos la lista de claves primarias de la sesions
		pks_list = request.session['ciudad']
		#obtenemos la lista de preguntas que hay en el array segun su id
		#parra ello le decimos, devuelveme todas las preguntas cuya clave primaria este en pks_List
		#__in hace que si hay una lista de id's, y esto ids son por ej: 1, 3, 7, va a buscar en la base de datos
		#la clave primaria que sea o 1 o 3 o 7, para ordenarlo convertimos 'Question.object.filter(pk__in=pks_list)' en una lista
		ciudad = list(Ciudad.objects.filter(pk__in=pks_list))
		#para que sea ordenada, creamos una clave de ordenado, en python cuando tenemos una lista y queremos ordenarla ejecutamos
		#la funcion sort() y le pasamos como clave una funcion que debe devolver un valor numerico que indica el valor de este
		#elemento de forma que pueda determinar si este elemento debe estar mas arriba o mas abajo
		#usamos una funcion lambda, que es una funcion anonima hecha de manera simplificada
		#añadimos lambda y como unico argumeto t: que lo que va hacer devolves es
		ciudad.sort(key=lambda t: pks_list.index(t.pk))
		result['ciudad'] = ciudad

	return result