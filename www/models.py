#encoding:utf-8
from django.db import models
from django.template import defaultfilters
from django.utils.text import slugify
from django.utils import timezone
from django.conf import settings
import os
# Create your models here.
class RedSocial(models.Model):
	nombre = models.CharField(max_length = 70, blank = False, verbose_name = 'Nombre de la Red Social' )
	slug_nombre = models.SlugField(max_length = 70, blank = False, verbose_name = 'Nombre para URL')
	web = models.URLField(max_length = 100, blank = False, verbose_name = 'Dirección web red social')
	icono = models.ImageField(upload_to = '', verbose_name='Icono')

	def save(self):
		self.slug_nombre = slugify(self.nombre)
		if self.icono:
			nombre, extension = os.path.splitext(self.icono.name)
			self.icono.name = self.slug_nombre + extension
		
		ruta_intermedia = self.slug_nombre

		if self.icono:
			icono_anterior = settings.MEDIA_ROOT + os.sep.join(['RedSocial', ruta_intermedia, 'imagen', self.icono.name])
			if os.path.isfile(icono_anterior):
				os.remove(icono_anterior)

		for field in self._meta.fields:
			if field.name == 'foto':
				field.upload_to = os.sep.join(['RedSocial', ruta_intermedia, 'imagen'])
		super(RedSocial, self).save()

	def __str__(self):
		return self.nombre.encode('utf-8')

class Pais(models.Model):
	nombre = models.CharField(max_length = 70, blank = False, verbose_name = 'Nombre del País' )
	bandera = models.ImageField(upload_to = '', blank = True, verbose_name='Imagen del País')
	escudo = models.ImageField(upload_to = '', blank = True, verbose_name='Escudo del País')

	def __str__(self):
		return self.nombre.encode('utf-8')

class Provincia(models.Model):
	nombre = models.CharField(max_length = 70, blank = True, verbose_name = 'Nombre de la Provincia' )
	pais = models.ForeignKey(Pais)

	def __str__(self):
		return self.nombre.encode('utf-8')

class Ciudad(models.Model):
	nombre = models.CharField(max_length = 70, blank = True, verbose_name = 'Nombre de la Ciudad' )
	slug_ciudad = models.SlugField(max_length = 70, blank = True, verbose_name = 'Nombre para URL')
	provincia = models.ForeignKey(Provincia)

	def save(self, *args, **kwargs):
		self.slug_ciudad = defaultfilters.slugify(self.nombre)
		super(Ciudad, self).save(*args, **kwargs)

	def __str__(self):
		return self.nombre.encode('utf-8')

class Partido(models.Model):
	nombre = models.CharField(max_length = 70, blank = False, verbose_name = 'Nombre del Partido')
	#slug_nombre = models.SlugField(max_length = 70, blank = False, verbose_name = 'Nombre para URL')
	fundacion =  models.DateTimeField(auto_now_add=False, verbose_name = 'Fecha de fundación')
	direccion = models.CharField(max_length = 160, blank = False, verbose_name = 'Dirección del Partido')
	telefono = models.CharField(max_length = 30, blank = False, verbose_name = 'Número telefónico del Partido')
	email = models.EmailField(max_length = 100, blank = False, verbose_name = 'Email del Partido')
	web = models.URLField(max_length = 100, blank = False, verbose_name = 'Dirección web del Partido')
	descripcion = models.CharField(max_length = 260, blank = False, verbose_name = 'Breve descripción del Partido')
	imagen = models.ImageField(upload_to = '', blank = True, verbose_name='Imagen del Partido')
	escudo = models.ImageField(upload_to = '', blank = True, verbose_name='Escudo del Partido')
	ciudad = models.ForeignKey(Ciudad)

	def __str__(self):
		return self.nombre.encode('utf-8')
"""
class Categoria(models.Model):
	nombre = models.CharField(max_length = 70, blank = False, verbose_name = 'Nombre de la Categoria')
	slug_nombre = models.SlugField(max_length = 70, blank = False, verbose_name = 'Nombre para URL')

	def __str__(self):
		return self.nombre.encode('utf-8')

class Cargo(models.Model):
	nombre = models.CharField(max_length = 70, blank = False, verbose_name = 'Nombre del Cargo')
	slug_nombre = models.SlugField(max_length = 70, blank = False, verbose_name = 'Nombre para URL')

	def __str__(self):
		return self.nombre.encode('utf-8')

class Rubro(models.Model):
	nombre = models.CharField(max_length = 70, blank = False, verbose_name = 'Nombre del Rubro')
	slug_nombre = models.SlugField(max_length = 70, blank = False, verbose_name = 'Nombre para URL')

	def __str__(self):
		return self.nombre.encode('utf-8')

class Miembro(models.Model):
	nombres = models.CharField(max_length = 70, blank = False, verbose_name = 'Nombres del Miembro')
	apellidos = models.CharField(max_length = 70, blank = False, verbose_name = 'Apellidos del Miembro')
	cargo = models.ForeignKey(Cargo)
	slug_nombre = models.SlugField(max_length = 70, blank = False, verbose_name = 'Nombre para URL')
	nacimiento =  models.DateTimeField(auto_now_add=False, verbose_name = 'Fecha de nacimiento del Miembro')
	direccion = models.CharField(max_length = 160, blank = False, verbose_name = 'Dirección del Miembro')
	telefono = models.CharField(max_length = 30, blank = False, verbose_name = 'Número telefónico del Miembro')
	email = models.EmailField(max_length = 100, blank = False, verbose_name = 'Email del Miembro')
	web = models.URLField(max_length = 100, blank = False, verbose_name = 'Dirección web del Miembro')
	descripcion = models.CharField(max_length = 260, blank = False, verbose_name = 'Breve descripción del Miembro')
	imagen = models.ImageField(upload_to = '', blank = True, verbose_name='Imagen del Miembro')

	def __str__(self):
		return self.nombres.encode('utf-8')

class DetallePrograma(models.Model):
	titulo = models.ForeignKey(Categoria)
	descripcion = models.CharField(max_length = 260, blank = False, verbose_name = 'Breve descripción del Programa')
	item = models.CharField(max_length = 200, blank = False, verbose_name = 'Item del programa')
	slug_nombre = models.SlugField(max_length = 70, blank = False, verbose_name = 'Nombre para URL')

	def __str__(self):
		return self.titulo.encode('utf-8')


class Programa(models.Model):
	imagen = models.ImageField(upload_to = '', blank = True, verbose_name='Imagen del Programa')
	titulo = models.CharField(max_length = 100, blank = False, verbose_name = 'Titulo del Programa')
	subtitulo = models.CharField(max_length = 160, blank = False, verbose_name = 'Subtitulo del programa')
	slug_nombre = models.SlugField(max_length = 70, blank = False, verbose_name = 'Nombre para URL')
	creado =  models.DateTimeField(auto_now_add=False, verbose_name = 'Fecha de creación del Programa')
	enlace = models.URLField(max_length = 100, blank = False, verbose_name = 'Dirección web del Programa')
	descripcion = models.CharField(max_length = 260, blank = False, verbose_name = 'Breve descripción del Programa')
	archivo = models.FileField(upload_to='', max_length=100)
	imagen = models.ImageField(upload_to = '', blank = True, verbose_name='Archivo del Programa')
	rubro = models.ForeignKey(Categoria)
	detalle = models.ForeignKey(DetallePrograma)

	def __str__(self):
		return self.titulo.encode('utf-8')
"""
class Candidato(models.Model):
	nombres = models.CharField(max_length = 70, blank = False, verbose_name = 'Nombres del Candidato')
	apellidos = models.CharField(max_length = 70, blank = False, verbose_name = 'Apellidos del Candidato')
	partido = models.ForeignKey(Partido)
	#slug_nombre = models.SlugField(max_length = 70, blank = False, verbose_name = 'Nombre para URL')
	nacimiento =  models.DateTimeField(verbose_name = 'Fecha de nacimiento del Candidato')
	direccion = models.CharField(max_length = 160, blank = False, verbose_name = 'Dirección del Candidato')
	telefono = models.CharField(max_length = 30, blank = False, verbose_name = 'Número telefónico del Candidato')
	email = models.EmailField(max_length = 100, blank = False, verbose_name = 'Email del Candidato')
	web = models.URLField(max_length = 100, blank = False, verbose_name = 'Dirección web del Candidato')
	descripcion = models.CharField(max_length = 260, blank = False, verbose_name = 'Breve descripción del Candidato')
	imagen = models.ImageField(upload_to = '', blank = True, verbose_name='Imagen del Candidato')
	#red_social = models.ForeignKey(RedSocial)

	def __str__(self):
		return self.nombres.encode('utf-8')
